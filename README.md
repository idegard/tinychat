# TinyChat

Pequeño chat basado en [Ruby on Rails](https://rubyonrails.org/) y demostrando [HOTWIRE](https://www.hotrails.dev/), demo utilizado en la charla Buk Tech Talk por [Idegard Olivares](https://gitlab.com/idegard).

![TinyChat screenshot](https://i.imgur.com/KMtZ6EU.jpg)

## Uso

1. Ejecuta con docker compose:

```bash
docker-compose up
```

2. Comienza a [jugar](http://localhost:3000) con el TinyChat que ya tiene algunos datos de prueba.
