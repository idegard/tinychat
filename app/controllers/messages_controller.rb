class MessagesController < ApplicationController
  before_action { cookies[:user] ||= SecureRandom.hex(10) }

  def create
    message_params = params.require(:message).permit(:room_id, :content)
    @message = Message.new(message_params.merge(room_id: params[:room_id], user: cookies[:user]))

    respond_to do |format|
      if @message.save
        format.html { redirect_to @message.room }
        format.turbo_stream
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def upvote
    @message = Message.find(params[:id])
    @message.increment(:votes)

    respond_to do |format|
      if @message.save
        format.html { redirect_to @message.room }
        format.turbo_stream{ turbo_stream.replace @message }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end
end
