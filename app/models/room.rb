class Room < ApplicationRecord
  has_many :messages, dependent: :destroy

  broadcasts_to :subscription_name

  private

  def subscription_name
    :all_rooms
  end
end
