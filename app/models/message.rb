class Message < ApplicationRecord
  belongs_to :room, touch: true

  broadcasts_to :room
end
