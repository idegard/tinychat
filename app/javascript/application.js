// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

document.addEventListener('turbo:before-stream-render', function (event) {
  if (event.target.action === 'append') {
    event.target.templateElement.content.firstElementChild.classList.add('animate__bounceIn', 'animate__animated', 'animate__faster')
  }
  if (event.target.action === 'replace') {
    event.target.templateElement.content.firstElementChild.classList.add('animate__headShake', 'animate__animated')
  }
})

document.addEventListener('turbo:frame-render', function(event){
  event.target.getElementsByTagName('div')[0].classList.add('animate__fadeIn', 'animate__animated', 'animate__faster')
})
