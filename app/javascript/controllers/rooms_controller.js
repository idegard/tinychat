import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ 'room', 'messagesPane' ]

  roomTargetConnected(_element){
    this.orderRooms.bind(this)()
    this.changeActive.bind(this)()
  }

  messagesPaneTargetConnected(_element){
    this.changeActive.bind(this)()
  }

  changeActive(){
    if(!this.hasMessagesPaneTarget) return

    this.roomTargets.forEach((room)=> {
      if(room.id === this.messagesPaneTarget.dataset.room){
        room.classList.add('bg-light')
      }else{
        room.classList.remove('bg-light')
      }
    })
  }

  orderRooms(){
    this.roomTargets.sort((a, b) => {
      if(b.dataset.order < a.dataset.order){
        b.before(a)
      }      
    })
  }
}
