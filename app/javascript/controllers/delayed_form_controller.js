import { Controller } from "@hotwired/stimulus"

export default class extends Controller {
  static targets = [ 'form', 'spinner', 'input' ]

  connect() {
    setTimeout(this.enableForm.bind(this), 3000)
  }

  enableForm(){
    this.spinnerTarget.classList.add('d-none')
    this.formTarget.classList.remove('d-none')
    this.inputTarget.focus()
  }
}
