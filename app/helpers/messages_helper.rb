module MessagesHelper
  def message_user_label(message)
    return '-' unless message

    source = "https://www.gravatar.com/avatar/#{ Digest::MD5.hexdigest(message.user || '') }?d=identicon&s=16"
    tag.img(nil, src: source, class: 'img-fluid rounded mx-1') + tag.span("#{message.user}:")
  end
end
