FROM ruby:3.1

RUN gem install bundler -v 2.3.3

RUN apt-get update ; \
    apt-get install -y build-essential wait-for-it

WORKDIR /tinychat

COPY . .

RUN bundle install

EXPOSE 3000

ENV PATH="/tinychat/bin:${PATH}"
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["puma"]
