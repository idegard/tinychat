#!/usr/bin/env bash
set -e
if [ "$1" = 'puma' ]
then
  bundle install
  wait-for-it $DB_HOST:3306 -s -t 60 -- bundle exec rails db:prepare
  bundle exec rails assets:precompile
  rm -f /tinychat/tmp/pids/server.pid
  trap 'pkill -f puma; pkill -f sidekiq; sleep 10' HUP INT QUIT TERM
  exec tail -f /dev/null &
  bundle exec rails s -b 0.0.0.0 &
  bundle exec sidekiq -t 5 &
  wait
else
  exec "$@"
fi